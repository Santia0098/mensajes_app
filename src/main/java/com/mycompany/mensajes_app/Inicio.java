/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.util.Scanner;

/**
 *
 * @author sorth_
 */
public class Inicio {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        int op = 0;
        
        do {
            
            System.out.println("------------------");
            System.out.println("Aplicacion de mensajes");
            System.out.println("1. Crear mensaje");
            System.out.println("2. Listar mensajes");
            System.out.println("3. Editar mensaje");
            System.out.println("4. Eliminar mensaje");
            System.out.println("5. Salir");
            
            op = sc.nextInt();
            
            switch(op) {
                case 1:
                    MensajesService.crearMensaje();
                    break;
                case 2:
                    MensajesService.listarMensajes();
                    break;
                case 3:
                    MensajesService.editarMensajes();
                    break;
                case 4:
                    MensajesService.borrarMensajes();
                    break;
                default: 
                    break;
                    
            }
            
        }while(op != 5);
        
    }
}
