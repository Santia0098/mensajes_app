/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

/**
 *
 * @author sorth_
 */
public class MensajesService {
    
    public static void crearMensaje() {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Escribe tu mensaje...");
        String mensaje = sc.nextLine();
        System.out.println("Ingresa el autor del mensaje");
        String autor = sc.nextLine();
        
        Mensajes msj = new Mensajes();
        msj.setMensaje(mensaje);
        msj.setAutor_mensaje(autor);
        
        MensajesDAO.crearMensajeDB(msj);
    }
    
    public static void listarMensajes(){
        MensajesDAO.leerMensajes();
    }
    public static void borrarMensajes () {
        Scanner sc = new Scanner(System.in);
        System.out.println("indica el ID del mensaje a borrar");
        int id_mensaje= sc.nextInt();
        MensajesDAO.borrarMensajeDB(id_mensaje);
    }
    
    public static void editarMensajes(){
        Scanner sc = new Scanner(System.in);
        System.out.println("escribe tu nuevo mensaje");
        String mensaje = sc.nextLine();
        
        System.out.println("indica el ID del mensaje a editar");
        int id_mensaje= sc.nextInt();
        Mensajes actualizacion = new Mensajes();
        actualizacion.setId_mensajes(id_mensaje);
        actualizacion.setMensaje(mensaje);
        MensajesDAO.actualizarMensajeDV(actualizacion);
    }
    
}
